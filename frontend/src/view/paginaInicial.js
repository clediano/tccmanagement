﻿import React, { Component } from 'react';

import { AppMenu } from './paginaInicial/menuLateral/AppMenu';
import { Switch } from 'react-router-dom';
import { ScrollPanel } from 'primereact/components/scrollpanel/ScrollPanel';
import { BreadCrumb } from 'primereact/components/breadcrumb/BreadCrumb';
import { criarIntensBreadcrumb } from './paginaInicial/breadcrumb';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { buscarMenu } from './paginaInicial/menuLateral/itens';

import Dashboard from '../template/Dashboard';
import FormaAvaliacao from '../view/cadastros/FormaAvaliacao/FormaAvaliacao';
import Estado from '../view/cadastros/Estado/CadastroEstado';

import classNames from 'classnames';
import AppTopbar from './paginaInicial/menuLateral/AppTopbar';
import AppInlineProfile from './paginaInicial/menuLateral/AppInlineProfile';
import PrivateRoute from '../components/Route/PrivateRoute';

import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'fullcalendar/dist/fullcalendar.css';
import 'font-awesome/css/font-awesome.min.css';
import 'primeflex/primeflex.css';
import 'primeicons/primeicons.css'
import '../app/App.css';
import Banca from './cadastros/Banca/Banca';

const styleTemplate = {
    padding: "8px",
    position: "fixed",
    left: "0",
    bottom: "50",
    width: "100%",
    zIndex: '1',
    backgroundColor: '#edf0f5',
    borderBottom: '1px solid #c8c8c8'
}

class InitialPage extends Component {

    constructor() {
        super();
        this.state = {
            layoutMode: 'static',
            layoutColorMode: 'light',
            staticMenuInactive: false,
            overlayMenuActive: false,
            mobileMenuActive: false,
            location: '',
            items: [],
            menu: undefined
        };

        this.onWrapperClick = this.onWrapperClick.bind(this);
        this.onToggleMenu = this.onToggleMenu.bind(this);
        this.onSidebarClick = this.onSidebarClick.bind(this);
        this.onMenuItemClick = this.onMenuItemClick.bind(this);
        this.alterarRota = this.alterarRota.bind(this)
        this.createMenu = this.createMenu.bind(this);
    }

    onWrapperClick(event) {
        if (!this.menuClick) {
            this.setState({
                overlayMenuActive: false,
                mobileMenuActive: false,
            })
        }

        this.menuClick = false;
    }

    componentDidUpdate(prevProps) {
        if (this.state.location !== window.location.hash) {
            this.setState({ ...this.state, location: window.location.hash, items: undefined })
        }

        if (this.state.items === undefined) {
            this.setState({ ...this.state, items: criarIntensBreadcrumb(window.location.hash, this.props.history) })
        }

        if (prevProps.dataAtualizacaoMenuLateral !== this.props.dataAtualizacaoMenuLateral) {
            this.createMenu();
        }
    }

    async componentWillMount() {

        this.setState({
            ...this.state,
            location: window.location.hash,
            items: criarIntensBreadcrumb(window.location.hash, this.props.history)
        })
    }

    componentDidMount() {
        this.createMenu();
    }

    onToggleMenu(event) {
        this.menuClick = true;

        if (this.isDesktop()) {
            if (this.state.layoutMode === 'overlay') {
                this.setState({
                    overlayMenuActive: !this.state.overlayMenuActive
                });
            }
            else if (this.state.layoutMode === 'static') {
                this.setState({
                    staticMenuInactive: !this.state.staticMenuInactive
                });
            }
        }
        else {
            const mobileMenuActive = this.state.mobileMenuActive;
            this.setState({
                mobileMenuActive: !mobileMenuActive
            });

            if (mobileMenuActive)
                this.removeClass(document.body, 'body-overflow-hidden');
            else
                this.addClass(document.body, 'body-overflow-hidden');
        }

        event.preventDefault();
    }

    onSidebarClick(event) {
        this.menuClick = true;
        setTimeout(() => {
            if (this.layoutMenuScroller) {
                this.layoutMenuScroller.moveBar();
            }
        }, 500);
    }

    onMenuItemClick(event) {
        if (!event.item.items) {
            this.setState({
                overlayMenuActive: false,
                mobileMenuActive: false
            })
        }
    }

    alterarRota(rota) {
        const locationAntigo = window.location.hash;

        this.props.history.push(rota);

        if (locationAntigo !== window.location.hash) {
            this.setState({
                ...this.state,
                items: []
            })
        }
    }

    createMenu() {
        this.setState({
            ...this.state,
            menu: buscarMenu(this.alterarRota)
        })
    }

    addClass(element, className) {
        if (element.classList) {
            element.classList.add(className);
        } else {
            element.className += ' ' + className;
        }
    }

    removeClass(element, className) {
        if (element.classList) {
            element.classList.remove(className);
        } else {
            element.className = element.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
        }
    }

    isDesktop() {
        return window.innerWidth > 1024;
    }

    render() {
        let wrapperClass = classNames('layout-wrapper', {
            'layout-overlay': this.state.layoutMode === 'overlay',
            'layout-static': this.state.layoutMode === 'static',
            'layout-static-sidebar-inactive': this.state.staticMenuInactive && this.state.layoutMode === 'static',
            'layout-overlay-sidebar-active': this.state.overlayMenuActive && this.state.layoutMode === 'overlay',
            'layout-mobile-sidebar-active': this.state.mobileMenuActive
        });
        let sidebarClassName = classNames("layout-sidebar", { 'layout-sidebar-dark': this.state.layoutColorMode === 'dark' });
        return (
            <React.Fragment>
                <div className={wrapperClass} onClick={this.onWrapperClick}>
                    <AppTopbar onToggleMenu={this.onToggleMenu} />
                    <div ref={(el) => this.sidebar = el} className={sidebarClassName} onClick={this.onSidebarClick}>

                        <ScrollPanel ref={(el) => this.layoutMenuScroller = el} style={{ height: '100%' }}>
                            <div className="layout-sidebar-scroll-content" >
                                <div style={{ marginBottom: '15px' }}>
                                    <AppInlineProfile />
                                </div>
                                <AppMenu model={this.state.menu} onMenuItemClick={this.onMenuItemClick} />
                            </div>
                        </ScrollPanel>
                    </div>

                    <div className="layout-main color-template" style={{ paddingTop: '50px', paddingBottom: '41px' }}>
                        <div className="layout-main color-template" style={styleTemplate}>
                            <BreadCrumb home={{ icon: 'pi pi-home', url: '#/' }} model={this.state.items} style={{ background: '#fff0', border: 'none' }} />
                        </div>
                    </div>

                    <div className="layout-main" style={{ paddingTop: '0px' }}>
                        <Switch>
                            <PrivateRoute
                                exact
                                path="/"
                                component={Dashboard}
                            />
                            <PrivateRoute
                                exact
                                path="/formas_avaliacao"
                                component={FormaAvaliacao}
                            />
                            <PrivateRoute
                                exact
                                path="/estados"
                                component={Estado}
                            />
                            <PrivateRoute
                                exact
                                path="/bancas"
                                component={Banca}
                            />
                        </Switch>
                    </div>

                    <div className="layout-mask"></div>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    typeReturnApi: state.app.typeReturnApi,
    dataAtualizacaoMenuLateral: state.menuLateral.dataAtualizacaoMenuLateral,
})

export default withRouter(connect(mapStateToProps)(InitialPage))