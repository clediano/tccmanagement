import React, { Component } from 'react';
import { Button } from 'primereact/button'
import { InputText } from 'primereact/inputtext';
import Col from '../../../components/Col'
import FormGroup from '../../../components/FormGroup';
import Grid from '../../../components/Grid';
import { Panel } from 'primereact/panel';
import { withFormik } from 'formik';
import * as Yup from 'yup';
import { mensagemError } from './style';
import { salvar, listar, editar, excluir } from './requests';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Growl } from 'primereact/growl';

const initialValues = {
    ibge: '',
    sigla: '',
    nome: ''
}

class Estado extends Component {

    constructor() {
        super();

        this.state = {
            selectedItem: null,
            estados: []
        };

        this.salvar = this.salvar.bind(this);
        this.editarItem = this.editarItem.bind(this);
        this.excluirItem = this.excluirItem.bind(this);
        this.renderAcoes = this.renderAcoes.bind(this);
    }

    async componentDidMount() {
        this.atualizarTabela();
    }

    async atualizarTabela() {
        await listar()
            .then(lista => {
                this.setState({ estados: lista.data });
            })
            .catch(resp => {
                this.growl.show({ severity: 'error', summary: resp.title, detail: resp.detail });
            })
        await this.setState({ selectedItem: null })
        this.props.setValues(initialValues);
        this.props.handleReset();
    }

    async salvar() {

        if (await this.validarFormulario()) {
            if (this.state.selectedItem) {
                this.handleEditar();
            } else {
                this.handleSalvar();
            }
        }
    }

    async validarFormulario() {

        await this.props.handleSubmit();
        await this.props.validateForm(this.props.values);

        if (Object.getOwnPropertyNames(this.props.errors).length > 0) {
            return false;
        }
        return true;
    }

    async handleEditar() {
        const { values } = this.props;
        await editar({ ...values, id: this.state.selectedItem.id })
            .then(() => {
                this.growl.show({ severity: 'success', summary: 'Sucesso!', detail: 'Registro alterado com sucesso!' });
                this.atualizarTabela();
            }).catch(resp => {
                this.growl.show({ severity: 'error', summary: resp.title, detail: resp.detail });
            })
    }

    async handleSalvar() {
        const { values } = this.props;
        await salvar(values)
            .then(() => {
                this.growl.show({ severity: 'success', summary: 'Sucesso!', detail: 'Registro adicionado com sucesso!' });
                this.atualizarTabela();
            }).catch(resp => {
                this.growl.show({ severity: 'error', summary: resp.title, detail: resp.detail });
            })
    }

    async editarItem(row) {
        await this.props.setValues(row);
        this.setState({ selectedItem: row })
    }

    async excluirItem(row) {
        await excluir(row)
            .then(() => {
                this.atualizarTabela();
                this.growl.show({ severity: 'success', summary: 'Sucesso!', detail: 'Registro foi deletado com sucesso!' });
            })
            .catch(resp => {
                this.growl.show({ severity: 'error', summary: resp.title, detail: resp.detail });
            })
    }

    renderAcoes(row) {
        return (
            <div>
                <Button type="button" icon="pi pi-trash" onClick={() => this.excluirItem(row)} className="p-button-danger" style={{ marginRight: '.5em' }} />
                <Button type="button" icon="pi pi-pencil" onClick={() => this.editarItem(row)} className="p-button-info" />
            </div>
        );
    }

    render() {

        const { values, touched, errors } = this.props;

        return (
            <Panel header="Cadastro de Estados" style={{ marginTop: '20px' }}>
                <Growl ref={(el) => this.growl = el} />
                <FormGroup>
                    <Grid>
                        <Col sm={12} md={3} lg={3} xl={3}>
                            <label>IBGE</label>
                            <InputText
                                onChange={e => this.props.setFieldValue('ibge', e.target.value)}
                                keyfilter="int"
                                value={values.ibge}
                                name="ibge"
                            />
                            {touched.ibge && errors.ibge && <p style={mensagemError}>{errors.ibge}</p>}
                        </Col>
                        <Col sm={12} md={6} lg={6} xl={6}>
                            <label>Nome</label>
                            <InputText
                                onChange={e => this.props.setFieldValue('nome', e.target.value)}
                                type="text"
                                value={values.nome}
                                name="nome"
                            />
                            {touched.nome && errors.nome && <p style={mensagemError}>{errors.nome}</p>}
                        </Col>
                        <Col sm={12} md={3} lg={3} xl={3}>
                            <label>Sigla</label>
                            <InputText
                                onChange={e => this.props.setFieldValue('sigla', String(e.target.value).toUpperCase())}
                                type="text"
                                keyfilter="alpha"
                                value={values.sigla}
                                name="sigla"
                            />
                            {touched.sigla && errors.sigla && <p style={mensagemError}>{errors.sigla}</p>}
                        </Col>
                    </Grid>
                    <Grid>
                        <Col sm={12} md={2} lg={2} xl={2}>
                            <Button
                                label={this.state.selectedItem ? "Alterar" : "Adicionar"}
                                type="button"
                                icon="pi pi-plus"
                                onClick={this.salvar}
                            />
                        </Col>
                    </Grid>
                </FormGroup>
                <DataTable
                    value={this.state.estados}
                    header="Listagem de Estados"
                >
                    <Column sortable field="id" header="#ID" />
                    <Column field="nome" header="Nome" />
                    <Column field="sigla" header="Sigla" />
                    <Column field="ibge" header="IBGE" />
                    <Column field="acoes" body={this.renderAcoes} style={{ textAlign: 'center' }} header="Ações" />
                </DataTable>
            </Panel>
        );
    }
}

export default withFormik({

    mapPropsToValues: () => (initialValues),

    validationSchema: Yup.object().shape({
        nome: Yup.string()
            .max(60, 'A descrição não pode ter mais de 60 caracteres.')
            .required('Descrição é um campo obrigatório.'),
        sigla: Yup.string()
            .max(2, 'A sigla não pode ter mais de 2 caracteres.')
            .required('A sigla é um campo obrigatório.')
    }),

    handleSubmit: () => { },

})(Estado);