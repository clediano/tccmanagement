import React, { Component } from 'react';
import { Button } from 'primereact/button'
import { InputText } from 'primereact/inputtext';
import Col from '../../../components/Col'
import FormGroup from '../../../components/FormGroup';
import Grid from '../../../components/Grid';
import { Panel } from 'primereact/panel';
import { withFormik } from 'formik';
import * as Yup from 'yup';
import { mensagemError } from './style';
import { salvar, listar, editar, excluir } from './requests';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Growl } from 'primereact/growl';
import { Calendar } from 'primereact/calendar';
import { Dropdown } from 'primereact/dropdown';

const initialValues = {
    data: '',
    horaInicio: '',
    horaFim: '',
    resultado: '',

}

class Banca extends Component {

    constructor() {
        super();

        this.state = {
            selectedItem: null,
            pesos: [],
            pessoas: []
        };

        this.salvar = this.salvar.bind(this);
        this.editarItem = this.editarItem.bind(this);
        this.excluirItem = this.excluirItem.bind(this);
        this.renderAcoes = this.renderAcoes.bind(this);
    }

    async componentDidMount() {
        this.atualizarTabela();
    }

    async atualizarTabela() {
        await listar()
            .then(lista => {
                this.setState({ pesos: lista.data });
            })
            .catch(resp => {
                this.growl.show({ severity: 'error', summary: resp.title, detail: resp.detail });
            })
        await this.setState({ selectedItem: null })
        this.props.handleReset();
    }

    async salvar() {
        if (await this.validarFormulario()) {
            if (this.state.selectedItem) {
                this.handleEditar();
            } else {
                this.handleSalvar();
            }
        }
    }

    async validarFormulario() {

        await this.props.handleSubmit();
        await this.props.validateForm(this.props.values);

        if (Object.getOwnPropertyNames(this.props.errors).length > 0) {
            return false;
        }
        return true;
    }

    async handleEditar() {
        const { values } = this.props;
        await editar({ ...values, id: this.state.selectedItem.id })
            .then(() => {
                this.growl.show({ severity: 'success', summary: 'Sucesso!', detail: 'Registro alterado com sucesso!' });
                this.atualizarTabela();
            }).catch(resp => {
                this.growl.show({ severity: 'error', summary: resp.title, detail: resp.detail });
            })
    }

    async handleSalvar() {
        const { values } = this.props;
        await salvar(values)
            .then(() => {
                this.growl.show({ severity: 'success', summary: 'Sucesso!', detail: 'Registro adicionado com sucesso!' });
                this.atualizarTabela();
            }).catch(resp => {
                this.growl.show({ severity: 'error', summary: resp.title, detail: resp.detail });
            })
    }

    async editarItem(row) {
        await this.props.setValues(row);
        this.setState({ selectedItem: row })
    }

    async excluirItem(row) {
        await excluir(row)
            .then(() => {
                this.atualizarTabela();
                this.growl.show({ severity: 'success', summary: 'Sucesso!', detail: 'Registro foi deletado com sucesso!' });
            })
            .catch(resp => {
                this.growl.show({ severity: 'error', summary: resp.title, detail: resp.detail });
            })
    }

    renderAcoes(row) {
        return (
            <div>
                <Button type="button" icon="pi pi-trash" onClick={() => this.excluirItem(row)} className="p-button-danger" style={{ marginRight: '.5em' }} />
                <Button type="button" icon="pi pi-pencil" onClick={() => this.editarItem(row)} className="p-button-info" />
            </div>
        );
    }

    hourFormat(data) {
        let hour = new Date(data).getHours();
        let minutes = new Date(data).getMinutes();

        if (hour < 10) hour = `0${hour}`;
        if (minutes < 10) minutes = `0${minutes}`;

        return `${hour}:${minutes}`;
    }

    dataFormat(data) {
        let day = new Date(data).getDate();
        let month = new Date(data).getMonth() + 1;
        let year = new Date(data).getFullYear();

        if (day < 10) day = `0${day}`;
        if (month < 10) month = `0${month}`;

        return `${day}/${month}/${year}`;
    }

    render() {

        const { values, touched, errors } = this.props;

        return (
            <Panel header="Banca de Trabalho de Conclusão de Curso" style={{ marginTop: '20px' }}>
                <Growl ref={(el) => this.growl = el} />
                <FormGroup>
                    <Grid>
                        <Col sm={12} md={12} lg={12} xl={12}>
                            <label>Aluno</label>
                            <Dropdown
                                value={values.pessoa}
                                options={this.state.pessoas}
                                onChange={(e) => this.props.setFieldValue('pessoa', e.value)}
                                filter={true}
                                autoWidth={false}
                                filterPlaceholder="Selecione..."
                                filterBy="label"
                                showClear={true}
                            />
                            {touched.pessoa && errors.pessoa && <p style={mensagemError}>{errors.pessoa}</p>}
                        </Col>
                    </Grid>
                    <Grid>
                        <Col sm={12} md={3} lg={3} xl={3}>
                            <label>Data da Banca</label>
                            <Calendar
                                dateFormat="dd/mm/yy"
                                value={values.data}
                                name="data"
                                onChange={(e) => this.props.setFieldValue('data', e.value)}

                            />
                            {touched.data && errors.data && <p style={mensagemError}>{errors.data}</p>}
                        </Col>
                        <Col sm={12} md={3} lg={3} xl={3}>
                            <label>Hora Inicial</label>
                            <Calendar
                                timeOnly={true}
                                showTime={true}
                                hourFormat="23"
                                name="horaInicio"
                                value={values.horaInicio}
                                onChange={(e) => this.props.setFieldValue('horaInicio', e.value)}
                            />
                            {touched.horaInicio && errors.horaInicio && <p style={mensagemError}>{errors.horaInicio}</p>}
                        </Col>
                        <Col sm={12} md={3} lg={3} xl={3}>
                            <label>Hora Final</label>
                            <Calendar
                                timeOnly={true}
                                showTime={true}
                                hourFormat="24"
                                name="horaFim"
                                value={values.horaFim}
                                onChange={(e) => this.props.setFieldValue('horaFim', this.hourFormat(e.value))}
                            />
                            {touched.horaFim && errors.horaFim && <p style={mensagemError}>{errors.horaFim}</p>}
                        </Col>
                        <Col sm={12} md={3} lg={3} xl={3}>
                            <label>Nota da Banca</label>
                            <InputText
                                onChange={e => this.props.setFieldValue('resultado', e.target.value)}
                                keyfilter="money"
                                value={values.resultado}
                                name="resultado"
                            />
                            {touched.resultado && errors.resultado && <p style={mensagemError}>{errors.resultado}</p>}
                        </Col>
                    </Grid>
                    <Grid>
                        <Col sm={12} md={2} lg={2} xl={2}>
                            <Button
                                label={this.state.selectedItem ? "Alterar" : "Adicionar"}
                                type="button"
                                icon="pi pi-plus"
                                onClick={this.salvar}
                            />
                        </Col>
                    </Grid>
                </FormGroup>
                <DataTable
                    value={this.state.pesos}
                    header="Listagem das bancas do aluno"
                >
                    <Column sortable field="id" header="#ID" />
                    <Column field="data" header="Data" />
                    <Column field="horaInicio" header="Hora Início" />
                    <Column field="horaFim" header="Hora Fim" />
                    <Column field="resultado" header="Nota" />
                    <Column field="acoes" body={this.renderAcoes} style={{ textAlign: 'center' }} header="Ações" />
                </DataTable>
            </Panel>
        );
    }
}

export default withFormik({

    mapPropsToValues: () => (initialValues),

    validationSchema: Yup.object().shape({
        data: Yup.date()
            .required('Data é um campo obrigatório.'),
        resultado: Yup.number()
            .max(10, 'A nota não pode ser maior de 10.')
            .positive('A nota não pode ser negativa.')
            .required('A nota é um campo obrigatório.')
    }),

    handleSubmit: () => { },

})(Banca);