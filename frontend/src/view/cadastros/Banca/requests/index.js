import axios from "axios";
import { API_URL } from '../../../../config/constants/api';

const URL = `${API_URL}/bancas`;

export async function listar() {
      return await axios.get(URL);
}

export async function salvar(dados) {
      return await axios.post(URL, dados);
}

export async function editar(dados) {
      return await axios.put(URL, dados);
}

export async function excluir(row) {
      return await axios.delete(URL + `/${row.id}`);
}