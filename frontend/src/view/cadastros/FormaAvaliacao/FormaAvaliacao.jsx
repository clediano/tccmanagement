import React, { Component } from 'react';
import { Button } from 'primereact/button'
import { InputText } from 'primereact/inputtext';
import Col from '../../../components/Col'
import FormGroup from '../../../components/FormGroup';
import Grid from '../../../components/Grid';
import { Panel } from 'primereact/panel';
import { withFormik } from 'formik';
import * as Yup from 'yup';
import { mensagemError } from './style';
import { salvar, listar, editar, excluir } from './requests';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Growl } from 'primereact/growl';

const initialValues = {
    descricao: '',
    peso: ''
}

class FormaAvaliacao extends Component {

    constructor() {
        super();

        this.state = {
            selectedItem: null,
            pesos: []
        };

        this.salvar = this.salvar.bind(this);
        this.editarItem = this.editarItem.bind(this);
        this.excluirItem = this.excluirItem.bind(this);
        this.renderAcoes = this.renderAcoes.bind(this);
    }

    async componentDidMount() {
        this.atualizarTabela();
    }

    async atualizarTabela() {
        await listar()
            .then(lista => {
                this.setState({ pesos: lista.data });
            })
            .catch(resp => {
                this.growl.show({ severity: 'error', summary: resp.title, detail: resp.detail });
            })
        await this.setState({ selectedItem: null })
        this.props.handleReset();
    }

    async salvar() {

        if (await this.validarFormulario()) {
            if (this.state.selectedItem) {
                this.handleEditar();
            } else {
                this.handleSalvar();
            }
        }
    }

    async validarFormulario() {

        await this.props.handleSubmit();
        await this.props.validateForm(this.props.values);

        if (Object.getOwnPropertyNames(this.props.errors).length > 0) {
            return false;
        }
        return true;
    }

    async handleEditar() {
        const { values } = this.props;
        await editar({ ...values, id: this.state.selectedItem.id })
            .then(() => {
                this.growl.show({ severity: 'success', summary: 'Sucesso!', detail: 'Registro alterado com sucesso!' });
                this.atualizarTabela();
            }).catch(resp => {
                this.growl.show({ severity: 'error', summary: resp.title, detail: resp.detail });
            })
    }

    async handleSalvar() {
        const { values } = this.props;
        await salvar(values)
            .then(() => {
                this.growl.show({ severity: 'success', summary: 'Sucesso!', detail: 'Registro adicionado com sucesso!' });
                this.atualizarTabela();
            }).catch(resp => {
                this.growl.show({ severity: 'error', summary: resp.title, detail: resp.detail });
            })
    }

    async editarItem(row) {
        await this.props.setValues(row);
        this.setState({ selectedItem: row })
    }

    async excluirItem(row) {
        await excluir(row)
            .then(() => {
                this.atualizarTabela();
                this.growl.show({ severity: 'success', summary: 'Sucesso!', detail: 'Registro foi deletado com sucesso!' });
            })
            .catch(resp => {
                this.growl.show({ severity: 'error', summary: resp.title, detail: resp.detail });
            })
    }

    renderAcoes(row) {
        return (
            <div>
                <Button type="button" icon="pi pi-trash" onClick={() => this.excluirItem(row)} className="p-button-danger" style={{ marginRight: '.5em' }} />
                <Button type="button" icon="pi pi-pencil" onClick={() => this.editarItem(row)} className="p-button-info" />
            </div>
        );
    }

    render() {

        const { values, touched, errors } = this.props;

        return (
            <Panel header="Forma de Avaliação" style={{ marginTop: '20px' }}>
                <Growl ref={(el) => this.growl = el} />
                <FormGroup>
                    <Grid>
                        <Col sm={12} md={7} lg={7} xl={7}>
                            <label>Descrição</label>
                            <InputText
                                size={100}
                                onChange={e => this.props.setFieldValue('descricao', e.target.value)}
                                type="text"
                                value={values.descricao}
                                name="descricao"
                            />
                            {touched.descricao && errors.descricao && <p style={mensagemError}>{errors.descricao}</p>}
                        </Col>
                        <Col sm={12} md={3} lg={3} xl={3}>
                            <label>Peso</label>
                            <InputText
                                size={10}
                                onChange={e => this.props.setFieldValue('peso', e.target.value)}
                                type="number"
                                value={values.peso}
                                name="peso"
                            />
                            {touched.peso && errors.peso && <p style={mensagemError}>{errors.peso}</p>}
                        </Col>
                        <Col sm={12} md={2} lg={2} xl={2}>
                            <Button
                                label={this.state.selectedItem ? "Alterar" : "Adicionar"}
                                type="button"
                                icon="pi pi-plus"
                                style={{ marginTop: '19px' }}
                                onClick={this.salvar}
                            />
                        </Col>
                    </Grid>
                </FormGroup>
                <DataTable
                    value={this.state.pesos}
                    header="Listagem das Formas de Avaliação"
                >
                    <Column sortable field="id" header="#ID" />
                    <Column field="descricao" header="Descrição" />
                    <Column field="peso" header="Peso" />
                    <Column field="acoes" body={this.renderAcoes} style={{ textAlign: 'center' }} header="Ações" />
                </DataTable>
            </Panel>
        );
    }
}

export default withFormik({

    mapPropsToValues: () => (initialValues),

    validationSchema: Yup.object().shape({
        descricao: Yup.string()
            .max(255, 'A descrição não pode ter mais de 255 caracteres.')
            .required('Descrição é um campo obrigatório.'),
        peso: Yup.number()
            .max(10, 'A nota não pode ser maior de 10.')
            .positive('A nota não pode ser negativa.')
            .required('A nota é um campo obrigatório.')
    }),

    handleSubmit: () => { },

})(FormaAvaliacao);