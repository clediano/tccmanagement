export default (state = {}, action) => {
    switch (action.type) {
        case 'MENU_LATERAL_ATUALIZAR':
        return {...state, dataAtualizacaoMenuLateral: new Date()}
        default:
            return state;
    }
}