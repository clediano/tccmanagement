export function buscarMenu(alterarRota) {
    const menu = [
        {
            label: 'Cadastros',
            icon: 'pi pi-fw pi-align-left',
            items: [
                {
                    label: 'Forma Avaliacao',
                    icon: 'pi pi-fw pi-pencil', command: () => { alterarRota("/formas_avaliacao") }
                },
                {
                    label: 'Cadastro de Estados',
                    icon: 'pi pi-fw pi-globe', command: () => { alterarRota("/estados") }
                },
            ]
        }, {
            label: 'Lançamentos',
            icon: 'pi pi-fw pi-th-large',
            items: [
                {
                    label: 'Banca',
                    icon: 'pi pi-fw pi-info', command: () => { alterarRota("/bancas") }
                },
            ]
        }
    ];
    return menu
}