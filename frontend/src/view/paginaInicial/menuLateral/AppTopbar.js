import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import { Menu } from 'primereact/menu';

class AppTopbar extends Component {

    constructor(props) {
        super(props)
        this.logoutClick = this.logoutClick.bind(this)
        this.textInput = React.createRef();
        this.state = {
            items: [
                { label: 'New', icon: 'pi pi-fw pi-plus', command: () => { window.location.hash = "/fileupload"; } },
                { label: 'Delete', icon: 'pi pi-fw pi-trash', url: 'http://primetek.com.tr' }

            ]
        };
    }

    static defaultProps = {
        onToggleMenu: null
    }

    static propTypes = {
        onToggleMenu: PropTypes.func.isRequired
    }

    logoutClick(e) {
        localStorage.clear();
        this.props.history.push('/login')
    }

    render() {
        return (
            <div>
                <Menu model={this.state.items} popup={true} ref={el => this.menuOpcoes = el} />
                <div className="layout-topbar clearfix" style={{ paddingTop: '0px' }}>
                    <a className="layout-menu-button" onClick={this.props.onToggleMenu} style={{ paddingTop: '10px' }}>
                        <span className="fa fa-bars" />
                        <div className="content-section implementation button-demo" />
                    </a>
                    <div className="layout-topbar-icons">
                        <a onClick={this.logoutClick}
                            className='layout-topbar-icon pi pi-sign-out'
                            style={{ fontSize: '2em', marginTop: '13px', marginLeft: '10px' }}>
                        </a>
                    </div>
                </div>
            </div>
        );
    }
}
export default withRouter(AppTopbar)