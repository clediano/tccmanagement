import React, { Component } from 'react';
import logo from '../../../app/logo/logo.png';
import { withRouter } from 'react-router'

const styleProfileName = {
    fontSize: '17px',
    fontWeight: '600',
    color: 'rgba(0, 0, 0, 0.54)',
};

class AppInlineProfile extends Component {

    constructor() {
        super();

        this.getUserName = this.getUserName.bind(this);
    }

    getUserName() {
        return localStorage.getItem('management@username')
    }

    render() {
        return (
            <div className="profile" style={{ paddingBottom: '0px' }}>
                <div style={{ cursor: 'pointer' }}>
                    <img src={logo} onClick={() => this.props.history.push('/')} alt="" style={{ width: '180px' }} />
                </div>
                <span className="username" style={styleProfileName}>{this.getUserName()}</span>
            </div>
        );
    }
}

export default withRouter(AppInlineProfile)