import React, { Component } from 'react';
import { Card } from 'primereact/card';
import { Button } from 'primereact/button';
import Col from '../../components/Col';
import Grid from '../../components/Grid';
import FormGroup from '../../components/FormGroup';
import { InputText } from 'primereact/inputtext';
import { Password } from 'primereact/password';
import { Checkbox } from 'primereact/checkbox';
import { style, styleButtonSubmit, styleHeaderLogo } from './style';
import { API_URL } from '../../config/constants/api';
import { Growl } from 'primereact/growl';
import axios from 'axios';

class Login extends Component {

      constructor() {
            super();

            this.state = {
                  checkbox: true,
                  login: '',
                  senha: ''
            };

            this.handleCheckbox = this.handleCheckbox.bind(this);
            this.handleSubmit = this.handleSubmit.bind(this);
      }

      async componentDidMount() {
            const value = localStorage.getItem('management@remember');
            const login = localStorage.getItem('management@username');
            const senha = localStorage.getItem('management@password');

            if (value) {
                  await this.setState({
                        ...this.state,
                        login: login,
                        senha: senha
                  });

                  this.handleSubmit();
            }
      }

      handleCheckbox = () => {
            this.setState({ checkbox: !this.state.checkbox })
      }

      handleLogin = ({ target }) => {
            this.setState({ login: target.value })
      }

      handlePassword = ({ target }) => {
            this.setState({ senha: target.value })
      }

      handleSubmit = () => {
            const { login, senha } = this.state;

            axios.get(`${API_URL}/usuario?login=${login}&senha=${senha}`)
                  .then(e => {
                        this.loginRequested(e);
                  }).catch(e => {
                        this.showMessage('error', 'Error de conexão!', 'Problemas ao conectar com o servidor. Por favor, tente novamente mais tarde!');
                  });
      }

      async onLoginError() {
            await this.showMessage('error', 'Error!', 'Usuário não autenticado!');
            this.setState({
                  ...this.state,
                  login: '',
                  senha: ''
            });
      }

      async onLoginSuccess() {
            const { login, senha } = this.state;

            localStorage.setItem('management@username', login);
            localStorage.setItem('management@password', senha);
            localStorage.setItem('management@remember', this.state.checkbox);

            this.props.history.push('/');
      }

      async loginRequested(resp) {
            if (resp.data === 'OK') {
                  await this.onLoginSuccess();
            } else {
                  await this.onLoginError();
            }
      }

      async showMessage(type, title, message) {
            await this.growl.show({ severity: type, summary: title, detail: message });
      }

      render() {
            const header = (
                  <img
                        alt="Logo"
                        style={styleHeaderLogo}
                        src='assets/layout/images/logo-tcc-management.png'
                  />
            );

            const footer = (
                  <FormGroup>
                        <Button
                              style={styleButtonSubmit}
                              label="Entrar"
                              className="p-button-raised"
                              onClick={this.handleSubmit}
                        />
                  </FormGroup>
            );

            return (

                  <React.Fragment>
                        <Growl ref={(el) => this.growl = el} />
                        <Card
                              className="ui-card-shadow"
                              subTitle=" Bem-Vindo ao TCCManagement"
                              style={style}
                              footer={footer}
                              header={header}
                        >
                              <FormGroup>
                                    <Grid>
                                          <Col>
                                                <label>Código</label>
                                                <InputText
                                                      keyfilter="int"
                                                      value={this.state.login}
                                                      onChange={this.handleLogin}
                                                />
                                          </Col>
                                    </Grid>
                                    <Grid>
                                          <Col>
                                                <label>Senha</label>
                                                <Password
                                                      value={this.state.senha}
                                                      onChange={this.handlePassword}
                                                      weakLabel="Senha fraca"
                                                      mediumLabel="Senha média"
                                                      strongLabel="Senha forte"
                                                      promptLabel="Informe sua senha"
                                                />
                                          </Col>
                                    </Grid>
                                    <Grid>
                                          <Col>
                                                <Checkbox
                                                      value={false}
                                                      onChange={this.handleCheckbox}
                                                      checked={this.state.checkbox}
                                                />
                                                <label className="p-checkbox-label">Lembrar de mim</label>
                                          </Col>
                                    </Grid>
                              </FormGroup>
                        </Card>
                  </React.Fragment >
            );
      }
}

export default Login;