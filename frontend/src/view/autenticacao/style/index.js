export const style = {
      marginLeft: 'auto',
      marginRight: 'auto',
      marginTop: '100px',
      width: '330px',
      height: '410px',
      borderRadius: '3px',
      padding: '5px'
}

export const styleButtonSubmit = {
      marginTop: '5px',
      height: '35px'
}

export const styleHeaderLogo = {
      width: '60%',
      marginLeft: '50px'
}