import React from 'react';
import ReactDOM from 'react-dom';

import App from './app/App';

import promise from 'redux-promise';
import multi from 'redux-multi';
import thunk from 'redux-thunk';

import { HashRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createStore, compose, applyMiddleware } from 'redux';

import 'babel-polyfill';

//reducers import
import rootReducers from './config/reducers/';

//configura extensão do redux pro navegador
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

//aplica os middlewars na aplicação e cria a store do redux
export const store = createStore(
    rootReducers,
    composeEnhancer(applyMiddleware(thunk, multi, promise))
);

ReactDOM.render((
    <Provider store={store}>
        <HashRouter hashType='slash'>
            <App />
        </HashRouter>
    </Provider>
), document.getElementById('root'));