import { combineReducers } from 'redux';
import appReducer from '../../app/AppReducer';
import menuLateralreducers from '../../view/paginaInicial/reducers';

const rootReducer = combineReducers({
      app: appReducer,
      menuLateral: menuLateralreducers,
})

export default rootReducer