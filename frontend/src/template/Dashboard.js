import React, { Component } from 'react';
import { countAlunos, countTccs, mediaTccs } from './requests';

class Dashboard extends Component {

    constructor() {
        super();

        this.state = {
            alunos: '',
            tcc: '',
            media: ''
        };
    }
    componentDidMount() {
        this.alimentarCards();
    }

    async alimentarCards() {
        await countAlunos().then(resp => this.setState({ alunos: resp.data }));
        await countTccs().then(resp => this.setState({ tcc: resp.data }));
        await mediaTccs().then(resp => this.setState({ media: resp.data }));
    }

    render() {

        return (
            <div className="p-grid p-fluid dashboard" style={{ marginTop: '15px' }}>
                <div className="p-col-12 p-lg-4">
                    <div className="card summary">
                        <span className="title">Alunos</span>
                        <span className="detail">Número de alunos cadastrados</span>
                        <span className="count visitors">{this.state.alunos}</span>
                    </div>
                </div>

                <div className="p-col-12 p-lg-4">
                    <div className="card summary">
                        <span className="title">TCC's</span>
                        <span className="detail">Número de TCC's apresentados</span>
                        <span className="count purchases">{this.state.tcc}</span>
                    </div>
                </div>

                <div className="p-col-12 p-lg-4">
                    <div className="card summary">
                        <span className="title">Nota</span>
                        <span className="detail">Média das notas dos TCC's</span>
                        <span className="count revenue">{this.state.media}</span>
                    </div>
                </div>

            </div>
        );
    }
}
export default Dashboard;