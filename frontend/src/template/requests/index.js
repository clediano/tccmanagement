import axios from "axios";

const API_URL = 'http://localhost:8080';

export async function countAlunos(){
      return await axios.get(`${API_URL}/pessoas/total`);
}

export async function countTccs(){
      return await axios.get(`${API_URL}/bancas/total`);
}

export async function mediaTccs(){
      return await axios.get(`${API_URL}/bancas/media`);
}