import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';

import Login from '../view/autenticacao/Login';
import Loading from '../components/Loading';
import PrivateRoute from '../components/Route/PrivateRoute';
import InitialPage from '../view/paginaInicial';

import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';
import '../layout/layout.css';

class App extends Component {

    constructor(props) {
        super(props);

        this.renderLoading = this.renderLoading.bind(this);

        this.state = {
            renderizarTela: false
        }
    }

    renderLoading() {
        if (this.props.typeReturnApi && typeof this.props.typeReturnApi === 'string') {

            if (this.props.typeReturnApi.indexOf('SUCCESS') !== -1 || this.props.typeReturnApi.indexOf('ERROR') !== -1) {
                return;
            } else if (this.props.typeReturnApi.indexOf('REQUEST') !== -1) {
                return <Loading />;
            }
        }
    }

    async componentWillMount() {
        this.setState({
            ...this.state,
            renderizarTela: true
        })
    }

    render() {
        if (this.state.renderizarTela) {
            return (
                <React.Fragment>
                    {this.renderLoading()}
                    <Switch>
                        <Route exact path='/login' component={Login} />
                        <PrivateRoute path="*" component={InitialPage} />
                    </Switch>
                </React.Fragment>
            );
        } else {
            return null;
        }
    }
}

const mapStateToProps = state => ({
    typeReturnApi: state.app.typeReturnApi,
})

export default withRouter(connect(mapStateToProps)(App))
