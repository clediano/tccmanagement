export default (state = {}, action) => {
    if (action.type.includes('REQUEST') || action.type.includes('SUCCESS') || action.type.includes('ERROR')) {
        return { ...state, typeReturnApi: action.type };
    } else {
        return state;
    }
}
