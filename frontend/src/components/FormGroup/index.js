import React, { Component } from 'react'

class FormGroup extends Component {
    render() {
        return (
            <div className="p-fluid">
                <div className="p-lg form-group">
                    {this.props.children}
                </div>
            </div>
        )
    }
}
export default FormGroup