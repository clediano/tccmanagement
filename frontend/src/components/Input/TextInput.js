import React, { Component } from 'react';
import { InputText } from 'primereact/inputtext';

class TextInput extends Component {
      constructor(props) {
            super(props);
            this.state = {}
      }
      render() {
            return (
                  <React.Fragment>
                        <label>{this.props.label}</label>
                        <br/>
                        <InputText
                              value={this.props.value}
                              onChange={this.props.onChange}
                              {...this.props}
                        />
                  </React.Fragment>
            );
      }
}

export default TextInput;