import React, { Component } from 'react';

class Col extends Component {

      toCssClasses({ sm, md, lg, xl }) {

            let classes = 'p-col-12 '

            classes += `p-sm-${sm ? sm : 12}`
            classes += ` p-md-${md ? md : 12}`
            classes += ` p-lg-${lg ? lg : 12}`
            classes += ` p-xl-${xl ? xl : 12}`

            return classes
      }

      render() {

            const gridClasses = this.toCssClasses(this.props)

            return (
                  <div className={gridClasses}>
                        {this.props.children}
                  </div>
            );
      }
}

/**
 * Exemplo de impl: <Col cols="12 12 12 12">1</Col>
 */

export default Col;