import React, { Component } from 'react';
import { ProgressSpinner } from 'primereact/components/progressspinner/ProgressSpinner';
import propTypes from 'prop-types';
import If from '../If';

const styleProgressSpinner = {
    zIndex: '1500',
    marginLeft: '50%',
    marginTop: '50px',
    position: 'absolute',
};

const msgStyle = {
    zIndex: '1500',
    marginLeft: '50%',
    marginTop: '150px',
    position: 'absolute',
    color: '#fff'
};

class Loading extends Component {
    render() {
        return (
            <React.Fragment>
                <If test={this.props.visible}>
                    <ProgressSpinner
                        id={this.props.id}
                        strokeWidth="5"
                        style={styleProgressSpinner}
                    />
                    <h2 style={msgStyle}>
                        Carregando...
                    </h2>
                </If>
            </React.Fragment>
        );
    }
}

Loading.propTypes = {
    /** Identificador do componente */
    id: propTypes.string,
    /** Identificador do componente */
    modalId: propTypes.string,
    /** Define se o componente está visível */
    visible: propTypes.bool
}

Loading.defaultProps = {
    visible: true
}

export default Loading;