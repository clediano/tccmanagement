import React, { Component } from 'react'
import { Route } from 'react-router-dom';
import { withRouter } from 'react-router'
import PropTypes from 'prop-types'

class PrivateRoute extends Component {

    constructor(props) {
        super(props)
        this.validarRota = this.validarRota.bind(this)
        this.getComponent = this.getComponent.bind(this)
        this.state = {
            hash: window.location.hash
        }
    }

    validarRota() {
        const user = localStorage.getItem('management@username');
        const pass = localStorage.getItem('management@password');

        if (!user || !pass) {
            this.props.history.push('/login')
        } else {
            return true;
        }
    }

    componentDidMount() {
        this.validarRota();
    }

    componentDidUpdate() {
        if (this.state.hash !== window.location.hash) {
            this.validarRota();
            this.setState({
                ...this.state,
                hash: window.location.hash
            })
        }
    }

    getComponent() {
        let componente = this.props.component;

        if (this.validarRota()) {
            return componente;
        }
    }

    render() {
        return (
            <React.Fragment>
                <Route
                    exact={this.props.exact}
                    path={this.props.path}
                    component={this.getComponent()}
                />
            </React.Fragment>
        )
    }
}

PrivateRoute.propTypes = {
    exact: PropTypes.bool,
    path: PropTypes.string,
    component: PropTypes.any,
}

export default withRouter(PrivateRoute)
