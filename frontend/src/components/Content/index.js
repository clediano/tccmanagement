import React, { Component } from 'react';
import { Card } from 'primereact/card';

const style = {
      padding: '10px'
}

class Content extends Component {
      render() {
            return (
                  <Card
                        title={this.props.title}
                        subTitle={this.props.subTitle}
                        style={style}
                  >
                        {this.props.children}
                  </Card>
            );
      }
}

export default Content;