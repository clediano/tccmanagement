import React, { Component } from 'react';

import { Dialog } from 'primereact/components/dialog/Dialog';

class Modal extends Component {

    render() {
        return (
            <React.Fragment>
                <div className="content-section implementation">
                    <Dialog
                        {...this.props}
                        header={<strong>{this.props.header}</strong>}
                        modal={true}
                    >
                        {this.props.children}
                    </Dialog>
                </div>
            </React.Fragment>
        );
    }
}

Modal.defaultProps = {
    width: '75%',
    dismissableMask: true
}

export default Modal;