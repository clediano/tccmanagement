import React, { Component } from 'react';

class Grid extends Component {

      render() {
            return (
                  <div className="p-grid" style={{ margin: '5px' }}>
                        {this.props.children}
                  </div>
            );
      }
}

export default Grid;