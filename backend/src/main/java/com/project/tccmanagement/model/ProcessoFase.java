package com.project.tccmanagement.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "processo_fase")
@Data
public class ProcessoFase implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pfa_id")
    private Long id;

    @Column(name = "pfa_numero_fases", nullable = false)
    private Integer numeroFase;

    @Column(name = "pfa_data_inicio")
    private Date dataInicio;

    @Column(name = "pfa_data_fim")
    private Date dataFim;

    @ManyToOne
    @JoinColumn(name = "fk_processo_id")
    private Processo processo;

    @ManyToOne
    @JoinColumn(name = "fk_banca_id")
    private Banca banca;

    @OneToOne
    @JoinColumn(name = "fk_trabalho_id")
    private Trabalho trabalho;

}
