package com.project.tccmanagement.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "coordenador")
public class Coordenador implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "coo_id")
    private Long id;

    @Column(name = "coo_salario")
    private Double salario;

    @OneToOne
    @JoinColumn(name = "fk_pessoa_id")
    private Pessoa pessoa;
}
