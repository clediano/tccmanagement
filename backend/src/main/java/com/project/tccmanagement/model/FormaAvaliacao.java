package com.project.tccmanagement.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "forma_avaliacao")
public class FormaAvaliacao implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fav_id")
    private Long id;

    @Column(name = "fav_descricao", nullable = false)
    private String descricao;

    @Column(name = "fav_peso")
    private Double peso;

}
