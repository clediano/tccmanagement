package com.project.tccmanagement.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import sun.util.calendar.BaseCalendar;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Data
@Table(name = "pessoa")
public class Pessoa implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pes_id")
    private Long id;

    @Column(name = "pes_nome", nullable = false, length = 60)
    private String nome;

    @Column (name = "pes_sexo")
    private Character sexo;

    @Column (name = "pes_nascimento")
    private Date nascimento;

    @ManyToOne
    @JoinColumn(name = "fk_cidade_id")
    private Cidade cidade;

    @JoinColumn(name = "fk_usuario_id")
    private Usuario usuario;

    @ManyToMany
    @JoinTable(name = "pessoa_endereco",
            joinColumns = @JoinColumn(name = "fk_pessoa_id"),
            inverseJoinColumns = @JoinColumn(name = "fk_endereco_id")
    )
    private List<Endereco> endereco;

    @ManyToMany(mappedBy = "pessoa", cascade = CascadeType.ALL)
    private List<Telefone> telefone;

    @JsonIgnore
    @Column(name = "banca")
    @ManyToMany(mappedBy = "pessoa")
    private List<Banca> banca;

    @ManyToMany(mappedBy = "pessoa", cascade = CascadeType.ALL)
    private List<Orientador> orientador;

    @ManyToMany(mappedBy = "pessoa", cascade = CascadeType.ALL)
    private List<Aluno> aluno;

    @ManyToMany(mappedBy = "pessoa", cascade = CascadeType.ALL)
    private List<Professor> professor;

}
