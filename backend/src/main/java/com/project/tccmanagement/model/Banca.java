package com.project.tccmanagement.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.swing.*;
import java.io.Serializable;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Timer;

@Entity
@Table(name = "banca")
@Data
public class Banca implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ban_id")
    private Long id;

    @Temporal(TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    @CreationTimestamp
    @Column(name = "ban_data", nullable = false)
    private Date data;

    @Column(name = "ban_hora_inicio")
    private Date horaInicio;

    @Column(name = "ban_hora_fim")
    private Date horaFim;

    @Column(name = "ban_resultado")
    private Double resultado;

    @Column(name = "ban_sala")
    private String sala;

    @ManyToMany
    @JoinTable(name = "banca_pessoa",
            joinColumns = @JoinColumn(name = "fk_banca_id"),
            inverseJoinColumns = @JoinColumn(name = "fk_pessoa_id")
    )
    private List<Pessoa> pessoa;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "fk_forma_avaliacao")
    private FormaAvaliacao formaAvaliacao;

}
