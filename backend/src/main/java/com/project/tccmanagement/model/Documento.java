package com.project.tccmanagement.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "documento")
@Data
public class Documento implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "doc_id")
    private Long id;

    @Lob
    @Column(name = "doc_arquivo")
    private byte[] arquivo;

    @ManyToOne
    @JoinColumn(name = "fk_processo_fase_id")
    private ProcessoFase processoFase;
}
