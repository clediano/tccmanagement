package com.project.tccmanagement.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@Table(name = "orientador")
public class Orientador implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ori_id")
    private Long id;

    @Column(name = "ori_salario")
    private Double salario;

    @OneToOne
    @JoinColumn(name = "fk_trabalho_ori_id")
    private Trabalho trabalho;

    @ManyToMany
    @JoinTable(name = "pessoa_orientador",
            joinColumns = @JoinColumn(name = "fk_orientador_id"),
            inverseJoinColumns = @JoinColumn(name = "fk_pessoa_id")
    )
    private List<Pessoa> pessoa;



}
