package com.project.tccmanagement.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@Table(name = "aluno")
public class Aluno implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "alu_id")
    private Long id;

    @Column(name = "alu_codigo")
    private Integer codigo;

    @ManyToOne
    @JoinColumn(name = "fk_orientador_id")
    private Orientador orientador;

    @ManyToMany
    @JoinTable(name = "trabalho_aluno",
            joinColumns = @JoinColumn(name = "fk_aluno_id"),
            inverseJoinColumns = @JoinColumn(name = "fk_trabalho_id")
    )
    private List<Trabalho> trabalho;

    @ManyToMany
    @JoinTable(name = "pessoa_aluno",
            joinColumns = @JoinColumn(name = "fk_aluno_id"),
            inverseJoinColumns = @JoinColumn(name = "fk_pessoa_id")
    )
    private List<Pessoa> pessoa;

}
