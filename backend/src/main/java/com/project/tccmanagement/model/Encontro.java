package com.project.tccmanagement.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "encontro")
@Data
public class Encontro implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "enc_id")
    private Long id;

    @Column(name = "enc_data_encontro", nullable = false)
    private Date data;

    @Column(name = "enc_observacao", length = 1000, nullable = false)
    @Size(max = 1000)
    private String observacao;

    @JoinColumn(name = "fk_processo_fase_id")
    private ProcessoFase processoFase;
}
