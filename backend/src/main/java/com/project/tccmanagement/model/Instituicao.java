package com.project.tccmanagement.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@Table(name = "instituicao")
public class Instituicao implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ins_id")
    private Long id;

    @Column(name = "ins_nome", nullable = false)
    private String nome;

    @Column(name = "ins_cnpj")
    private String cnpj;

    @Column(name = "curso")
    @ManyToMany(mappedBy = "instituicao")
    private List<Curso> curso;

    @OneToOne
    @JoinColumn(name = "fk_endereco")
    private Endereco endereco;
}
