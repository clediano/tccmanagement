package com.project.tccmanagement.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@Table(name = "endereco")
public class Endereco implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "end_id")
    private Long id;

    @Column(name = "end_rua")
    private String rua;

    @Column(name = "end_bairro")
    private String bairro;

    @JsonIgnore
    @ManyToMany(mappedBy = "endereco")
    private List<Pessoa> pessoa;

}
