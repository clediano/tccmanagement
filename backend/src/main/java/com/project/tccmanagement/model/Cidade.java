package com.project.tccmanagement.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "cidade")
@Data
public class Cidade implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "cid_id")
    private Long id;

    @Column(name = "cid_codigo", nullable = false)
    private Long codigo;

    @Column(name = "cid_nome", length = 60, nullable = false)
    private String nome;

    @ManyToOne
    @JoinColumn(name = "fk_estado_id")
    private Estado estado;
}
