package com.project.tccmanagement.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@Table(name = "processo")
public class Processo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pes_id")
    private Long id;

    @Column(name = "pro_numero_fases", nullable = false)
    private Integer numroFases;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "processo")
    private List<ProcessoFase> processoFase;

}