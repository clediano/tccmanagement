package com.project.tccmanagement.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "pais")
public class Pais implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pai_id")
    private Long id;

    @Column(name = "pai_nome", nullable = false)
    private String nome;

    @Column(name = "pai_ddi")
    private String ddi;
}
