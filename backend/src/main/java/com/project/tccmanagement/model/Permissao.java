package com.project.tccmanagement.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@Table(name = "permissao")
public class Permissao implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nome;

    @ManyToMany
    @JoinTable(name = "permissao_grupo",
            joinColumns = @JoinColumn(name = "fk_permissao_id"),
            inverseJoinColumns = @JoinColumn(name = "fk_grupo_id")
    )
    private List<Grupo> grupos;
}
