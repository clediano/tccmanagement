package com.project.tccmanagement.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@Table(
        name = "grupo",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"nome"})
        }
)
public class Grupo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nome;

    private String descricao;

    @ManyToMany(mappedBy = "grupos")
    private List<Usuario> usuarios;

    @ManyToMany(mappedBy = "grupos")
    private List<Permissao> permissoes;
}
