package com.project.tccmanagement.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@Table(name = "professor")
public class Professor implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pes_id")
    private Long id;

    @Column(name = "pro_salario")
    private Double salario;

    @ManyToMany
    @JoinTable(name = "pessoa_professor",
            joinColumns = @JoinColumn(name = "fk_professor_id"),
            inverseJoinColumns = @JoinColumn(name = "fk_pessoa_id")
    )
    private List<Pessoa> pessoa;
}
