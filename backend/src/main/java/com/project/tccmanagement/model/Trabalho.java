package com.project.tccmanagement.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@Table(name = "trabalho")
public class Trabalho implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "tra_titulo", nullable = false)
    private String titulo;

    @Column(name = "tra_resumo", nullable = false)
    private String resumo;

    @ManyToMany(mappedBy = "trabalho", cascade = CascadeType.ALL)
    private List<Aluno> aluno;

    @OneToOne
    @JoinColumn(name = "fk_orientador_id")
    private Orientador orientador;

    @OneToOne
    @JoinColumn(name = "fk_tra_processo")
    private Processo processo;
}
