package com.project.tccmanagement.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@Table(name = "telefone")
public class Telefone implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "tel_id")
    private Long id;

    @Column(name = "tel_ddd", length = 2)
    @Size(max = 2)
    private String ddd;

    @Column(name = "tel_numero", nullable = false, length = 10)
    @Size(max = 10)
    private String numero;

    @ManyToMany
    @JoinTable(name = "telefone_pessoa",
        joinColumns = @JoinColumn(name = "fk_telefone_id"),
        inverseJoinColumns = @JoinColumn(name = "fk_pessoa_id"))
    private List<Pessoa> pessoa;
}
