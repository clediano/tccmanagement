package com.project.tccmanagement.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@Table(name = "area_conhecimento")
public class AreaConhecimento implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "aco_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "aco_nome")
    private String nome;

    @Column(name = "aco_descricao")
    private String descricao;

    @ManyToMany(mappedBy = "areasConhecimento")
    private List<Curso> curso;

}
