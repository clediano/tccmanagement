package com.project.tccmanagement.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "estado")
@Data
public class Estado implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "est_ibge")
    private String ibge;

    @Column(name = "est_sigla", nullable = false, length = 2)
    private String sigla;

    @Column(name = "est_nome", nullable = false, length = 60)
    private String nome;

    public void setSigla(String sigla) {
        this.sigla = sigla.toUpperCase();
    }
}
