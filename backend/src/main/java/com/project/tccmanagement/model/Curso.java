package com.project.tccmanagement.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@Table(name = "curso")
public class Curso implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cur_id")
    private Long id;

    @Column(name = "cur_nome")
    private String nome;

    @Column(name = "cur_duracao", nullable = false)
    private String duracao;

    @ManyToMany
    @JoinTable(name = "curso_area_conhecimento",
            joinColumns = @JoinColumn(name = "fk_curso_id"),
            inverseJoinColumns = @JoinColumn(name = "fk_area_conhecimento_id")
    )
    private List<AreaConhecimento> areasConhecimento;

    @ManyToMany
    @JoinTable(name = "curso_instituicao",
            joinColumns = @JoinColumn(name = "fk_curso_id"),
            inverseJoinColumns = @JoinColumn(name = "fk_instituicao_id")
    )
    private List<Instituicao> instituicao;

}
