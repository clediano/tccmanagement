package com.project.tccmanagement.repository;

import com.project.tccmanagement.model.Telefone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "telefones", path = "telefones")
public interface TelefoneRepository extends JpaRepository<Telefone, Long> {
}
