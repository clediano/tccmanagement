package com.project.tccmanagement.repository;

import com.project.tccmanagement.model.Pessoa;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "pessoas", path = "pessoas")
public interface PessoaRepository extends JpaRepository<Pessoa, Long> {
    @Query(value = "SELECT count(*) from pessoa where pessoa.pes_aluno = true;", nativeQuery = true)
    Integer countAlunos();
}
