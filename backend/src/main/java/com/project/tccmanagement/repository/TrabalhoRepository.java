package com.project.tccmanagement.repository;

import com.project.tccmanagement.model.Trabalho;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "trabalhos", path = "trabalhos")
public interface TrabalhoRepository extends JpaRepository<Trabalho, Long> {
}