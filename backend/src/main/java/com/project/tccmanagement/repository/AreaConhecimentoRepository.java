package com.project.tccmanagement.repository;

import com.project.tccmanagement.model.AreaConhecimento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "areasConhecimento", path = "areas_conhecimento")
public interface AreaConhecimentoRepository extends JpaRepository<AreaConhecimento, Long> {
}
