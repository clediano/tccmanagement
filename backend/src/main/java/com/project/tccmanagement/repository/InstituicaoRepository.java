package com.project.tccmanagement.repository;

import com.project.tccmanagement.model.Instituicao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "instituicoes", path = "instituicoes")
public interface InstituicaoRepository extends JpaRepository<Instituicao, Long> {
}
