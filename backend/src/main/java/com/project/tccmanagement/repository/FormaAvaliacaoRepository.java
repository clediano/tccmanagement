package com.project.tccmanagement.repository;

import com.project.tccmanagement.model.FormaAvaliacao;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.awt.print.Pageable;

@RepositoryRestResource(collectionResourceRel = "formasAvaliacao", path = "formas_avaliacao")
public interface FormaAvaliacaoRepository extends JpaRepository<FormaAvaliacao, Long> {

}
