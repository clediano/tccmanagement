package com.project.tccmanagement.repository;

import com.project.tccmanagement.model.Grupo;
import com.project.tccmanagement.model.Permissao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "permissoes", path = "permissoes")
public interface PermissaoRepository extends JpaRepository<Permissao, Long> {

    List<Permissao> findByGruposIn(Grupo grupo);

}