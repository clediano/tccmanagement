package com.project.tccmanagement.repository;

import com.project.tccmanagement.model.Pais;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "paises", path = "paises")
public interface PaisRepository extends JpaRepository<Pais, Long> {
}
