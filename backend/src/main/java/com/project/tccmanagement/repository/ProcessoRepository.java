package com.project.tccmanagement.repository;

import com.project.tccmanagement.model.Processo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "processos", path = "processos")
public interface ProcessoRepository extends JpaRepository<Processo, Long> {
}
