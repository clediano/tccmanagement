package com.project.tccmanagement.repository;

import com.project.tccmanagement.model.Banca;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "bancas", path = "bancas")
public interface BancaRepository extends JpaRepository<Banca, Long> {
    @Query(value = "SELECT count(*) from banca;", nativeQuery = true)
    Integer countTccs();

    @Query(value = "select (SELECT sum(ban_resultado) from banca) / (SELECT count(*) from banca);", nativeQuery = true)
    Float mediaTccs();
}