package com.project.tccmanagement.repository;

import com.project.tccmanagement.model.Encontro;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "encontros", path = "encontros")
public interface EncontroRepository extends JpaRepository<Encontro, Long> {
}