package com.project.tccmanagement.repository;

import com.project.tccmanagement.model.Endereco;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "enderecos", path = "enderecos")
public interface EnderecoRepository extends JpaRepository<Endereco, Long> {
}
