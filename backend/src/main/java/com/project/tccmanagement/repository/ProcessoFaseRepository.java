package com.project.tccmanagement.repository;

import com.project.tccmanagement.model.ProcessoFase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "processosFase", path = "processos_fase")
public interface ProcessoFaseRepository extends JpaRepository<ProcessoFase, Long> {
}