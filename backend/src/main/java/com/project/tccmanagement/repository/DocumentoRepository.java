package com.project.tccmanagement.repository;

import com.project.tccmanagement.model.Documento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "documentos", path = "documentos")
public interface DocumentoRepository extends JpaRepository<Documento, Long> {
}