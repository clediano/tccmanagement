package com.project.tccmanagement.repository;

import com.project.tccmanagement.model.Grupo;
import com.project.tccmanagement.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "grupos", path = "grupos")
public interface GrupoRepository extends JpaRepository<Grupo, Long> {

    List<Grupo> findByUsuariosIn(Usuario usuario);
}
