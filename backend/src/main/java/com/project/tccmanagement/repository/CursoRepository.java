package com.project.tccmanagement.repository;

import com.project.tccmanagement.model.Curso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "cursos", path = "cursos")
public interface CursoRepository extends JpaRepository<Curso, Long>{
}
