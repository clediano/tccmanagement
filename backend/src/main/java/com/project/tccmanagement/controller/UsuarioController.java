package com.project.tccmanagement.controller;

import com.project.tccmanagement.model.Usuario;
import com.project.tccmanagement.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioController {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @GetMapping(value = "/usuario")
    public HttpStatus getUsuario(@RequestParam("login") String login, @RequestParam("senha") String senha) {
        Usuario user = usuarioRepository.findByLogin(login);

        if (user != null) {
            if (new BCryptPasswordEncoder().matches(senha, user.getSenha())) {
                return HttpStatus.OK;
            }
        }
        return HttpStatus.NOT_FOUND;
    }
}
