package com.project.tccmanagement.controller;

import com.project.tccmanagement.repository.BancaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/pessoas")
public class PessoaController {

    @Autowired
    private BancaRepository bancaRepository;

    @GetMapping("/total")
    public Integer countTccs() {
        return bancaRepository.countTccs();
    }

    @GetMapping("/media")
    public Float mediaTccs() {
        return bancaRepository.mediaTccs();
    }
}
