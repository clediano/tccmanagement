package com.project.tccmanagement.controller;

import com.project.tccmanagement.error.ResourceNotFoundException;
import com.project.tccmanagement.model.FormaAvaliacao;
import com.project.tccmanagement.repository.FormaAvaliacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/formas_avaliacao")
public class FormaAvaliacaoController {

    @Autowired
    FormaAvaliacaoRepository formaAvaliacaoRepository;

    @PostMapping
    public FormaAvaliacao salvar(@Valid @RequestBody FormaAvaliacao formaAvaliacao) {
        return formaAvaliacaoRepository.save(formaAvaliacao);
    }

    @GetMapping
    public List<FormaAvaliacao> listar() {
        return formaAvaliacaoRepository.findAll();
    }

    @GetMapping("/{id}")
    public FormaAvaliacao listarUm(@PathVariable Long id) {
        return formaAvaliacaoRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Registro não encontrado"));
    }

    @DeleteMapping("/{id}")
    public void deletar(@PathVariable Long id) {

        if (verificarRegistro(id)) {
            formaAvaliacaoRepository.deleteById(id);
        }
    }

    @PutMapping
    public FormaAvaliacao atualizar(@Valid @RequestBody FormaAvaliacao formaAvaliacao) {

        if (verificarRegistro(formaAvaliacao.getId())) {
            return formaAvaliacaoRepository.save(formaAvaliacao);
        } else {
            return formaAvaliacao;
        }
    }

    private Boolean verificarRegistro(Long id) {
        FormaAvaliacao formaAvaliacao = formaAvaliacaoRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Registro não encontrado"));
        if (formaAvaliacao != null) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }
}
