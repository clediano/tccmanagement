package com.project.tccmanagement.controller;

import com.project.tccmanagement.error.ResourceNotFoundException;
import com.project.tccmanagement.model.Estado;
import com.project.tccmanagement.repository.EstadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/estados")
public class EstadoController {

    @Autowired
    EstadoRepository estadoRepository;

    @PostMapping
    public Estado salvar(@Valid @RequestBody Estado estado) {
        return estadoRepository.save(estado);
    }

    @GetMapping
    public List<Estado> listar() {
        return estadoRepository.findAll();
    }

    @GetMapping("/{id}")
    public Estado listarUm(@PathVariable Long id) {
        return estadoRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Registro não encontrado"));
    }

    @DeleteMapping("/{id}")
    public void deletar(@PathVariable Long id) {

        if (verificarRegistro(id)) {
            estadoRepository.deleteById(id);
        }
    }

    @PutMapping
    public Estado atualizar(@Valid @RequestBody Estado estado) {

        if (verificarRegistro(estado.getId())) {
            return estadoRepository.save(estado);
        } else {
            return estado;
        }
    }

    private Boolean verificarRegistro(Long id) {
        Estado estado = estadoRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Registro não encontrado"));
        if (estado != null) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

}
