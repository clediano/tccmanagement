package com.project.tccmanagement.controller;

import com.project.tccmanagement.model.Banca;
import com.project.tccmanagement.model.Pessoa;
import com.project.tccmanagement.repository.BancaRepository;
import com.project.tccmanagement.repository.PessoaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/bancas")
public class BancaController {

    @Autowired
    private BancaRepository bancaRepository;

    @Autowired
    private PessoaRepository pessoaRepository;

    @GetMapping
    public List<Banca> getAll() {
        return bancaRepository.findAll();
    }

    @PostMapping
    public Banca salvar(@Valid @RequestBody Banca banca) {
        return bancaRepository.save(banca);
    }

    @GetMapping("/total")
    public Integer countTccs() {
        return bancaRepository.countTccs();
    }

    @GetMapping("/media")
    public Float mediaTccs() {
        return bancaRepository.mediaTccs();
    }

}
