package com.project.tccmanagement.security;

import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class UsuarioSistema extends User {

    private static final long serialVersionUID = 1L;

    @Getter
    private String nome;

    public UsuarioSistema(String nome, String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);

        this.nome = nome;
    }

}
